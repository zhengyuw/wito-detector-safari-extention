"use strict";

//npm install gulp gulp-minify-css gulp-uglify gulp-clean gulp-cleanhtml gulp-jshint gulp-strip-debug gulp-zip gulp-prompt --save-dev

const gulp = require("gulp");
const readlineSync = require("readline-sync");
const clean = require("gulp-clean");
const cleanhtml = require("gulp-cleanhtml");
const stripdebug = require("gulp-strip-debug");
const terser = require("gulp-terser");
const zip = require("gulp-zip");
const fs = require("fs");
const jeditor = require("gulp-json-editor");

let applicationsData, applicationSelectionIndex;
let environmentSelectionIndex, customEnvironmentUrl;
const environments = ["Production", "Staging", "Custom"];

gulp.task("select-task", async function (done) {
    const promise = new Promise(async (resolve, reject) => {
        const response = await require("axios").get(
            "https://y5apldnvr7.execute-api.eu-west-1.amazonaws.com/staging/applications"
        );
        applicationsData = response.data;
        applicationSelectionIndex = readlineSync.keyInSelect(
            applicationsData.map((app) => app.name),
            "Choose application to build"
        );
        environmentSelectionIndex = readlineSync.keyInSelect(
            environments,
            "Choose environment",
            { defaultInput: "Staging" }
        );

        if (environments[environmentSelectionIndex] === "Custom") {
            customEnvironmentUrl = readlineSync.question("Enter custom url: ");
        }

        resolve();
    });

    return promise;
});

const getSelectedApplication = () => {
    return applicationsData[applicationSelectionIndex];
};

const getSelectedEnvironment = () => {
    return environments[environmentSelectionIndex];
};

//clean build directory
gulp.task("clean", function () {
    return gulp.src("build/*", { read: false }).pipe(clean());
});

gulp.task("load-general-icons", function () {
    const defaultIconPath = `ext/icons`;

    return gulp
        .src([`${defaultIconPath}/*.png`, `${defaultIconPath}/*.svg`])
        .pipe(gulp.dest("build/ext/icons"));
});

gulp.task("load-level-icons", function () {
    const defaultIconPath = `ext/icons`;

    return gulp
        .src(`${defaultIconPath}/levels/*`)
        .pipe(gulp.dest("build/ext/icons/levels"));
});

gulp.task("load-category-icons", function () {
    const application = getSelectedApplication();
    const buildConfigFilePath = `./ext/config/buildConfig.json`;

    // Load the icons for the active categories
    var buildConfig = require(buildConfigFilePath);
    const categoryPaths = [];
    for (let activeLabel of application.config.activeLabels) {
        const filename = buildConfig.categoryToIcon[activeLabel]
            .split(/(\\|\/)/g)
            .pop();
        const categoryPath = `ext/icons/categories/${filename}`;
        if (fs.existsSync(categoryPath)) {
            categoryPaths.push(categoryPath);
        } else {
            console.warn(
                `The category icon for ${activeLabel} does not exist at ${categoryPath}`
            );
        }
    }
    return gulp
        .src(categoryPaths)
        .pipe(gulp.dest("build/ext/icons/categories"));
});

gulp.task("load-edition-icons", async function () {
    const application = getSelectedApplication();
    const defaultIconPath = `ext/icons`;

    // Load the application/edition specific icons
    const applicationIconPath = `${defaultIconPath}/editions/${application.config.edition.toLowerCase()}`;
    if (fs.existsSync(applicationIconPath)) {
        return gulp
            .src(`${applicationIconPath}/*`)
            .pipe(gulp.dest(`build/${applicationIconPath}`));
    } else {
        console.warn(
            `Warning the icon path "${applicationIconPath}" for edition ${application.config.edition} does not exist`
        );
        return;
    }
});

gulp.task("application-config", async function () {
    const application = getSelectedApplication();
    const configFilePath = `ext/config/config.json`;
    const buildConfigFilePath = `./ext/config/buildConfig.json`;

    let environmentSelection = getSelectedEnvironment();
    let environmentUrl = customEnvironmentUrl;

    if (environmentSelection !== "Custom") {
        environmentUrl =
            application.config.endpoints[environmentSelection.toLowerCase()];
    }

    // Get the paths for the active categories
    var buildConfig = require(buildConfigFilePath);
    const activeCategoryIcons = {};
    for (let activeLabel of application.config.activeLabels) {
        try {
            activeCategoryIcons[activeLabel] =
                buildConfig.categoryToIcon[activeLabel];
        } catch (err) {
            console.log(
                `The active category ${activeLabel} does not have a matching icon specified in ${buildConfigFilePath}.`
            );
            throw err;
        }
    }

    if (fs.existsSync(configFilePath)) {
        return gulp
            .src(configFilePath)
            .pipe(
                jeditor((json) => {
                    json.remote.BASE_URL = environmentUrl;
                    json.activeLabels = application.config.activeLabels;
                    json.categoryToIcon = activeCategoryIcons;
                    json.edition = application.config.edition;
                    return json;
                })
            )
            .pipe(gulp.dest("build/ext/config"));
    } else {
        console.error(
            `Warning the config path "${configFilePath}" for application ${application.name} does not exist`
        );
        throw new Error(
            `Warning the config path "${configFilePath}" for application ${application.name} does not exist`
        );
    }
});

gulp.task("manifest", function () {
    return gulp
        .src("ext/manifest.json")
        .pipe(
            jeditor({
                name: getSelectedApplication().name,
            })
        )
        .pipe(gulp.dest("build/ext"));
});

//copy and clean HTML files
gulp.task("html", function () {
    return gulp.src("ext/*.html").pipe(cleanhtml()).pipe(gulp.dest("build"));
});

gulp.task("scripts", function () {
    let scriptsSource = gulp.src(["ext/js/**/*.js"]);

    if (getSelectedEnvironment() === "Production") {
        console.log("Removing logs and obfuscating code...");
        scriptsSource = scriptsSource.pipe(stripdebug()).pipe(
            terser({
                keep_fnames: false,
                mangle: true,
            })
        );
    }

    return scriptsSource.pipe(gulp.dest("build/ext/js"));
});

//minify styles
gulp.task("styles", function () {
    return gulp.src("src/styles/**").pipe(gulp.dest("build/styles"));
});

gulp.task("static", function () {
    return gulp
        .src([
            "ext/**",
            "!ext/icons/**",
            "!ext/config/**",
            "!ext/js/**",
            "!ext/manifest.json",
        ])
        .pipe(gulp.dest("build/ext"));
});

gulp.task("zip", function () {
    var manifest = require("./ext/manifest"),
        distFileName = manifest.name + " v" + manifest.version + ".zip",
        mapFileName = manifest.name + " v" + manifest.version + "-maps.zip";

    return gulp
        .src(["build/**"])
        .pipe(zip(distFileName))
        .pipe(gulp.dest("dist"));
});

gulp.task(
    "default",
    gulp.series(
        "select-task",
        "clean",
        "static",
        "load-general-icons",
        "load-level-icons",
        "load-category-icons",
        "load-edition-icons",
        "application-config",
        "html",
        "scripts",
        "styles",
        "manifest",
        "zip",
        (cb) => {
            console.log(
                `Built application "${
                    getSelectedApplication().name
                }" for environment "${getSelectedEnvironment()}"`
            );
            cb();
        }
    )
);
