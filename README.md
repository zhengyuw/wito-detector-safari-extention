# WITO - What is true online

A browser extension for both Chrome and Mozilla-based browsers, **WITO** searches all links on a given webpage for references to unreliable sources, checking against a manually compiled list of domains. It then provides visual warnings about the presence of questionable links or the browsing of questionable websites:

Example domain classifications (in flux) include:

-   **Press**: Verified news sites.
-   **Dark Pattern**: Sites with Dark Patterns.
-   **Råd & Rön**: Sites or companies that are blacklisted by Råd & Rön.
-   **Casino**: Sites that focus on gambling and betting.
-   **General blacklist**: Sites found on smaller or unofficial blacklists.
-   **Malware**: Sites that contain malicious software that is intentionally designed to cause damage to a computer, client, server, or computer network.
-   **Phishing**: Sites that try to manipulate users to emit sensitive information, such as passwords and credit card details.

## Table of Content

- [WITO - What is true online](#wito---what-is-true-online)
  - [Table of Content](#table-of-content)
  - [Add the plugin to the browser:](#add-the-plugin-to-the-browser)
    - [Chrome](#chrome)
    - [Firefox](#firefox)
    - [Edge](#edge)
  - [Build the Plugin](#build-the-plugin)
    - [Setup & Installation](#setup--installation)
    - [Build](#build)
    - [Output](#output)
  - [Add the Build Version to the Browser](#add-the-build-version-to-the-browser)
    - [Prerequisites](#prerequisites)
    - [Chrome](#chrome-1)
    - [Firefox](#firefox-1)
    - [Edge](#edge-1)
  - [Other](#other)
    - [The Config File](#the-config-file)
    - [Flowchart](#flowchart)

## Add the plugin to the browser:

**Note**: Firefox currently has its own version of the plugin. Use the branch _firefox-version_ for firefox.

### Chrome

-   Navigate to <chrome://extensions>
-   Enable developer mode slider in top right
-   Press "Load unpacked"
-   Open the ext folder in WITO root

WITO plugin should now be added

### Firefox

-   Navigate to <about:debugging#/runtime/this-firefox>
-   Press "Load temporary plugin"
-   Open to the ext folder and select the manifest file

WITO plugin should now be added

### Edge

-   Navigate to <edge://extensions/>
-   Enable developer mode slider located in bottom left
-   Press "Load unpacked plugin"
-   Open to the ext folder of the manifest

WITO plugin should now be added

## Build the Plugin

It is often sufficient to use the non-build version of the plugin during development. For distribution, however, you need to use the build version.

### Setup & Installation

To build the application install gulp globally from a terminal. This step is not necessary if using node_modules.

```
npm install -g gulp
```

navigate to the root folder of the application and run

```
npm install
```

### Build

To build the application, run gulp in the root folder. Use following if you installed Gulp globally:

```
gulp
```

else use:

```
/node_modules/.bin/gulp
```

When you build the plugin you will be asked to select application and environment:

-   **Application**: Choose _MyGuard_ to build the default edition of the plugin.
-   **Environment**: Choose _staging_ or _production_. Note that _staging_ is a test environment, thus the classified sites and categories you may see are also for testing (and not actual blacklisted/verified sites).

**Note:** The script uses a remote endpoint as information source when building the application. To add a new application insert a new entry in the database, along with corresponding app configuration.

**Note:** When building for **production**, the code will be obfuscated and log prints removed.

### Output

The built project can be found in the folder _build_.

A **zip**-file can be found in the folder _dist_.

## Add the Build Version to the Browser

> :warning: **By using the build/production version** the extension uses the remote endpoint for fetching data. See "Build the Plugin" for more information and additional setup.

### Prerequisites

The application has to be built first. Follow the instructions in section: [Build the Plugin](#build-the-plugin).

The built application will be in the **build** folder.

### Chrome

-   Navigate to <chrome://extensions>
-   Enable developer mode slider in the top right
-   Press "Load unpacked"
-   Open the **build/ext** folder in WITO root

WITO plugin should now be added

### Firefox

-   Navigate to <about:debugging#/runtime/this-firefox>
-   Press "Load temporary plugin"
-   Open to the **build/ext** folder and select the manifest file

WITO plugin should now be added

### Edge

-   Navigate to <edge://extensions/>
-   Enable developer mode slider located in bottom left
-   Press "Load unpacked plugin"
-   Open to the **build/ext** folder of the manifest

WITO plugin should now be added

## Other

### The Config File

The config file ([ext/config/config.json](#ext/config/config.json)) specifies different configurations for the plugin. If you build the plugin this file is **not** used, so you can ignore it (it will be created automatically in the build-folder).

You can make these configurations:

-   **Remote**: Specifies the environment, the endpoint at the BASE_URL is the one used. Change this to the staging URL, production URL, or the dev URL depending on which environment you want to run.
-   **Edition**: Specifies which edition of the plugin to use. _Myguard_ is the default edition.
-   **Active categories**: Specifies which categories are active.

### Flowchart

![alt text](./flow.png)

---
