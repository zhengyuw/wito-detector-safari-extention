const witoHTML = {
    witoClassificationInfo: {},
    edition: "myguard",
    categoryToIcon: {},
    windowSize: {
        width: null,
        height: null,
    },

    setWindowSize: () => {
        // https://javascript.info/size-and-scroll-window

        witoHTML.windowSize.width = Math.max(
            document.body.scrollWidth,
            document.documentElement.scrollWidth,
            document.body.offsetWidth,
            document.documentElement.offsetWidth,
            document.body.clientWidth,
            document.documentElement.clientWidth
        );
        witoHTML.windowSize.height = Math.max(
            document.body.scrollHeight,
            document.documentElement.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.offsetHeight,
            document.body.clientHeight,
            document.documentElement.clientHeight
        );
    },

    getAllLinkElements: () => {
        const allLinks = $('a[href]:not([href^="#"])');
        return allLinks.toArray();
    },

    getLinkElementsNotChecked: () => {
        let links = $("a[href]").not('[href^="#"]').not("[wito-href]");
        links = links.toArray();
        return links;
    },

    getGmailElements: () => {
        let links = $('span[email*="@"]').not("[wito-href]");
        links = links.toArray();
        return links;
    },

    getOutlookElements: () => {
        let links = $('span[title*="@"]').not("[wito-href]");
        links = links.toArray();
        return links;
    },

    setAttribute: (links, attribute, extractor) => {
        links.forEach((link) => {
            const [element, value] = extractor(link);
            $(element).attr(attribute, value);
        });
    },

    setLinkCheckedAttribute: (links, extractor) => {
        const keyAttribute = "wito-href";
        witoHTML.setAttribute(links, keyAttribute, extractor);
    },

    setLinkFlaggedAttribute: (links, extractor) => {
        const keyAttribute = "wito-flagged";
        witoHTML.setAttribute(links, keyAttribute, extractor);
    },

    flagLinks: (links, anchorExtract) => {
        links
            .filter((link) => link.getAttribute("wito-flagged"))
            .map((link) => witoHTML.flagLink(link, anchorExtract));
    },

    flagLink: (element, anchorExtract) => {
        const linkAnchor = anchorExtract(element);
        let flaggedLabels = element.getAttribute("wito-flagged").split(",");
        const labelInfo = witoHTML.getLabelInfo(flaggedLabels);
        witoFlagging.flagLink($(element), linkAnchor, labelInfo);
    },

    getFlagLinkAnchor: (siteInfo) => {
        return (link) => {
            switch (getDomainName(siteInfo.siteDomain)) {
                case "twitter":
                    return $(link);
                //return $($(link).closest('article'));

                case "_facebook":
                    if ($(link).parents(".mtm").length > 0) {
                        return $($(link).closest(".mtm"));
                    }
                    break;

                case "_google":
                    const gParents = $(link).parents(".rc");
                    if (gParents.length > 0) {
                        return $(link); //.closest('.rc');
                    }
                    break;
            }

            return $(link);
        };
    },

    flagSite: (types) => {
        const labelInfo = witoHTML.getLabelInfo(types);
        witoFlagging.flagSite(labelInfo);
    },

    getTypeFromElement: (element) => {
        return Array.isArray(element["wito-info"].categories)
            ? element["wito-info"].categories
            : element["wito-info"].categories;
    },

    hasActiveCategory: (categories) => {
        return categories.some(
            (category) =>
                witoHTML.witoClassificationInfo.categories[category] !==
                undefined
        );
    },

    isActiveCategory: (category) => {
        return (
            witoHTML.witoClassificationInfo.categories[category] !== undefined
        );
    },

    getLabelInfo: (categoryIds) => {
        categories = {};
        return categoryIds
            .map((categoryId) => {
                const categoryDetails =
                    witoHTML.witoClassificationInfo.categories[categoryId];
                if (categoryDetails) {
                    return { ...categoryDetails, id: categoryId };
                }
            })
            .filter((info) => info);
    },

    connectMutationObserver: (callback) => {
        const observerRoot = $("body");
        const observerFilter = [{ element: "div" }];
        observerRoot.mutationSummary("connect", callback, observerFilter);
    },

    disconnectMutationObserver: () => {
        const observerRoot = $("body");
        observerRoot.mutationSummary("disconnect");
    },

    clearLinks: () => {
        $("[wito-href]").each(function () {
            $(this).removeAttr("wito-href wito-flagged");
        });
        $("div.wito-alert-popup").remove();
        $("div.wito-alert").remove();
    },
};
