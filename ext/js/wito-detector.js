createSiteInfo = (domainInfo, url) => {
    return {
        siteLabeled: domainInfo ? true : false,
        siteUrl: url2Path(url),
        siteDomain: url2Domain(url),
        categories: domainInfo ? domainInfo.categories : "",
        labeledUrl: domainInfo ? domainInfo.labeledUrl : "",
    };
};

const getSiteInfo = async (url) => {
    const domainInfo = await backgroundAPI.getDomainInfo(url);
    const siteInfo = createSiteInfo(domainInfo, url);
    console.log("Site info", siteInfo);
    return siteInfo;
};

const getWitoInfo = async (language) => {
    const allWitoInfo = await backgroundAPI.getWitoLabels();
    const configFile = await backgroundAPI.getConfigFile();

    witoHTML.witoClassificationInfo = {
        categories: await filterActiveLabels(
            allWitoInfo.categories,
            language,
            configFile
        ),
        levels: filterActiveLanguage(allWitoInfo.levels, language),
    };
    witoHTML.edition = configFile.edition;
    witoHTML.categoryToIcon = configFile.categoryToIcon;
    const shorts = await backgroundAPI.getShorts();
    const witoInfo = createWitoInfo(shorts);
    return witoInfo;
};

const createWitoInfo = (shorts) => {
    return {
        shorts: shorts,
    };
};

const filterActiveLabels = async (labels, language, configFile) => {
    const dbCategoryIds = Object.keys(labels);
    const activeLabels = configFile.activeLabels.filter((category) => {
        if (dbCategoryIds.some((dbCategory) => dbCategory === category)) {
            return true;
        } else {
            console.warn(
                "Warning: The active category does not exist in the database"
            );
            return false;
        }
    });
    return activeLabels.reduce((result, key) => {
        result[key] = labels[key][language];
        return result;
    }, {});
};

const filterActiveLanguage = (levels, language) => {
    return Object.keys(levels).reduce((result, key) => {
        result[key] = levels[key][language];
        return result;
    }, {});
};

const flagSite = (siteInfo, witoInfo) => {
    const labels = Array.isArray(siteInfo.categories)
        ? siteInfo.categories
        : [siteInfo.categories];
    console.log(labels);
    const filteredLabels = labels.filter(witoHTML.isActiveCategory);
    if (filteredLabels.length > 0) {
        witoHTML.flagSite(filteredLabels);
    }
};

const handlePage = (siteInfo, witoInfo) => {
    const linkHandlers = [
        new ShortLinkHandler(witoInfo.shorts),
        new FBLinkHandler(),
        new EmailLinkHandler(),
        new PhoneNumberHandler(),
        new StandardLinkHandler(),
    ];
    const pageHandlers = [
        new CustomEmailPageHandler(new GmailPageInfo(), siteInfo),
        new CustomEmailPageHandler(new OutlookPageInfo(), siteInfo),
        new StandardPageHandler(linkHandlers, siteInfo),
    ];

    const linkPromises = pageHandlers
        .filter((handler) => handler.matches(siteInfo))
        .flatMap((pageHandler) => pageHandler.getLinks());

    Promise.all(linkPromises).then((links) => {
        const filteredLinks = removeParentLinks(
            filterOnCategory(links.flat()),
            siteInfo
        );
        console.log("Number of new flagged links: ", filteredLinks.length);
        flagLinks(filteredLinks, siteInfo);
    });
};

const removeParentLinks = (links, siteInfo) => {
    return links.filter(
        (link) =>
            !(
                siteInfo.siteLabeled &&
                isSubUrl(link["wito-info"].labeledUrl, siteInfo.labeledUrl)
            )
    );
};

const filterOnCategory = (links) => {
    const typeFilter = (witoLink) => {
        const categories = witoHTML.getTypeFromElement(witoLink);
        return witoHTML.hasActiveCategory(categories);
    };
    return links.filter(typeFilter);
};

const flagLinks = (flaggedLinks, siteInfo) => {
    const extractor = (elem) => [elem["element"], elem["wito-info"].categories];
    witoHTML.setLinkFlaggedAttribute(flaggedLinks, extractor);
    const flaggedElements = flaggedLinks.map((link) => link["element"]);
    witoHTML.flagLinks(flaggedElements, witoHTML.getFlagLinkAnchor(siteInfo));
};

const clearLinks = () => {
    console.log("clear links");
    witoHTML.clearLinks();
};

const clearModals = () => {
    manageModal.resetModal();
    manageBannerModal.resetModal();
};

const mutationChanged = (siteInfo, witoInfo) => {
    console.log("Mutation callback called");
    witoHTML.disconnectMutationObserver();

    if (siteInfo.siteUrl !== url2Path(window.location.href)) {
        clearLinks();
        clearModals();
        init();
    } else {
        handlePage(siteInfo, witoInfo);
        startMutationListener(siteInfo, witoInfo);
    }
};

const startMutationListener = (siteInfo, witoInfo) => {
    const callbackFn = () => mutationChanged(siteInfo, witoInfo);
    witoHTML.connectMutationObserver(callbackFn);
};

const init = async () => {
    const language = await backgroundAPI.getStoredLanguage();
    const siteInfo = await getSiteInfo(window.location.href);
    const witoInfo = await getWitoInfo(language);
    if (siteInfo.siteLabeled) {
        flagSite(siteInfo, witoInfo);
    }

    handlePage(siteInfo, witoInfo);
    startMutationListener(siteInfo, witoInfo);
};

const start = () => {
    if (window === window.top) {
        setTimeout(() => {
            witoHTML.setWindowSize();
            $(window).resize(() => {
                witoHTML.setWindowSize();
            });
        }, 700);

        init();
        browser.runtime.onMessage.addListener(function (message) {
            switch (message.operation) {
                case "languageChange":
                    console.log("Language change: ", message);
                    witoHTML.disconnectMutationObserver();
                    clearLinks();
                    clearModals();
                    init();
                    break;
            }
        });
    }
};

if (typeof chrome !== "undefined") {
    browser = chrome;
}

$(document).ready(() => {
    start();
});
