const manageBannerModal = {
    isModalOpen: false,

    openModal: (labelInfo) => {
        if (!manageBannerModal.isModalOpen) {
            $("body").append('<div class="wito-banner-blur"></div>');
            $(".wito-banner-blur").on("click", () => {
                manageBannerModal.closeModal();
            });
            witoModal.insertBannerModal(
                labelInfo,
                manageBannerModal.closeModal
            );
            manageBannerModal.isModalOpen = true;
        }
    },

    closeModal: () => {
        witoModal.removeBannerModal();
        $(`.wito-banner-blur`).remove();
        manageBannerModal.isModalOpen = false;
    },

    resetModal: () => {
        if (manageBannerModal.isModalOpen) {
            manageBannerModal.closeModal();
        }
    },
};
