const manageModal = {
    CLOSE_DELAY: 600,

    isModalOpen: false,
    openModalInfo: {
        activeCloseTimeout: null,
        linkElement: null,
    },

    onPopupMouseEnter: (linkElement, labelInfo, popupPosition, popupWidth) => {
        if (
            manageModal.isModalOpen &&
            isSamePosition(manageModal.openModalInfo.linkElement, linkElement)
        ) {
            manageModal.clearCloseTimeout();
        } else if (
            manageModal.isModalOpen &&
            !isSamePosition(manageModal.openModalInfo.linkElement, linkElement)
        ) {
            manageModal.closeModal();
        }
        if (!manageModal.isModalOpen) {
            manageModal.openModal(
                linkElement,
                labelInfo,
                popupPosition,
                popupWidth
            );
        }
    },

    onPopupMouseLeave: (linkElement) => {
        if (
            manageModal.isModalOpen &&
            isSamePosition(manageModal.openModalInfo.linkElement, linkElement)
        ) {
            manageModal.resetCloseTimeout();
        } else {
            console.warn("This should never happen");
        }
    },

    openModal: (linkElement, labelInfo, popupPosition, popupWidth) => {
        manageModal.isModalOpen = true;
        manageModal.openModalInfo.linkElement = linkElement;
        linkElement.addClass("breathing color");
        $("body").append('<div class="wito-blur"></div>');
        witoModal.insertModal(
            labelInfo,
            () => manageModal.closeModal(),
            popupPosition,
            popupWidth
        );
        $(".wito-modal").mouseenter(() => {
            manageModal.clearCloseTimeout();
        });
        $(".wito-modal").mouseleave(() => {
            manageModal.closeModal();
        });
    },

    resetModal: () => {
        if (manageModal.isModalOpen) {
            manageModal.closeModal();
        }
    },

    closeModal: () => {
        prevLinkElement = manageModal.openModalInfo.linkElement;
        manageModal.isModalOpen = false;
        manageModal.clearCloseTimeout();
        manageModal.openModalInfo.linkElement = null;
        prevLinkElement.removeClass("breathing color");
        $(".wito-blur").remove();
        witoModal.removeModal($(".wito-modal"));
    },

    clearCloseTimeout: () => {
        activeTimeout = manageModal.openModalInfo.activeCloseTimeout;
        if (activeTimeout !== null) {
            clearTimeout(activeTimeout);
            manageModal.openModalInfo.activeCloseTimeout = null;
        }
    },

    resetCloseTimeout: () => {
        manageModal.clearCloseTimeout();
        modalTimeout = setTimeout(() => {
            manageModal.closeModal();
        }, manageModal.CLOSE_DELAY);
        manageModal.openModalInfo.activeCloseTimeout = modalTimeout;
    },
};
