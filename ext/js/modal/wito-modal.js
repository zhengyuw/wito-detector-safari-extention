const witoModal = {
    insertModal: (labelInfo, onClose, popupPosition, popupWidth) => {
        $("body").append(`<dialog class=wito-modal></dialog>`);
        const modalElement = $(`.wito-modal`);
        witoModal.initModal(modalElement, labelInfo, onClose);

        positionCss = witoModal.calcPosition(
            modalElement,
            popupPosition,
            popupWidth
        );

        modalElement.css(positionCss);
    },

    insertBannerModal: (labelInfo, onClose) => {
        $("body").append(`<dialog class=wito-banner-modal></dialog>`);
        const modalElement = $(`.wito-banner-modal`);
        witoModal.initModal(modalElement, labelInfo, onClose);
    },

    removeBannerModal: () => {
        $(`.wito-banner-modal`).remove();
    },

    removeModal: (modal) => {
        modal.remove();
    },

    calcPosition: (modal, popupPosition, popupWidth) => {
        const windowHeight = witoHTML.windowSize.height
            ? witoHTML.windowSize.height
            : Infinity;
        const windowWidth = witoHTML.windowSize.width
            ? witoHTML.windowSize.width
            : Infinity;
        const xPos = witoModal.getFittedLeftPos(
            modal.width(),
            windowWidth,
            popupWidth,
            popupPosition.left
        );
        const yPos = witoModal.getFittedTopPos(
            modal.height(),
            windowHeight,
            popupPosition.top
        );
        return {
            top: yPos + "px",
            left: xPos + "px",
            right: "auto",
        };
    },

    getFittedLeftPos: (modalWidth, windowWidth, popupWidth, popupLeftPos) => {
        const distToPopup = modalWidth * 0.08;
        let xPos = popupLeftPos + popupWidth + distToPopup;
        if (xPos < 0) {
            return 0;
        }
        if (xPos + modalWidth > windowWidth) {
            let leftSidePos = popupLeftPos - (modalWidth + distToPopup);
            if (leftSidePos < 0) {
                return Math.max(windowWidth - modalWidth, 0);
            }

            return leftSidePos;
        }

        return xPos;
    },

    getFittedTopPos: (modalHeight, windowHeight, popupTopPos) => {
        let yPos = popupTopPos - modalHeight / 2;
        if (yPos < 0) {
            return 0;
        }
        if (yPos + modalHeight > windowHeight) {
            return Math.max(windowHeight - modalHeight, 0);
        }
        return yPos;
    },

    initModal: (modal, labelInfo, onClose) => {
        witoModal.addHeader(modal, onClose);
        witoModal.addBody(modal, labelInfo);
        witoModal.addFooter(modal);
    },

    addHeader: (modal, onClose) => {
        modal.append(`<header class="wito-modal-header" ></header>`);
        const modalHeader = $(`.wito-modal-header`);
        witoModal.addLogoIcon(modalHeader, modal);
        witoModal.addCloseButton(modalHeader, onClose);
    },

    addCloseButton: (header, onClose) => {
        header.append(
            `<button class="wito-button wito-modal-close-button"><img class="wito-modal-close-icon" src=${chrome.extension.getURL(
                "../icons/close_black.png"
            )}></img></button>`
        );
        $(".wito-modal-close-button").on("click", onClose);
    },

    addLogoIcon: (header) => {
        if (witoHTML.edition === "lrf") {
            header.append(
                `<div class="wito-modal-partnership-header"><img class="wito-partner-logo" src=${chrome.extension.getURL(
                    `icons/editions/${witoHTML.edition}/logo.svg`
                )}></img><img class="wito-poweredby-logo" src=${chrome.extension.getURL(
                    "icons/myguard-powered-by.svg"
                )}></img></div>`
            );
        } else {
            header.append(
                `<img class="wito-logo-icon" src=${chrome.extension.getURL(
                    "icons/myguard-black.svg"
                )}></img>`
            );
        }
    },

    addBody: (modal, labelInfo) => {
        const labels = witoModal.getLabels(labelInfo);
        modal.append(labels);
    },

    getLabels: (labelInfo) => {
        let labelContainer = $(
            `<section class="wito-modal-label-table"></section>`
        );
        labelInfo.sort((label1, label2) => label1.level - label2.level);
        const labels = labelInfo.map((info) => {
            const labelRow = $(`<article class="wito-label"></article>`);
            witoModal.addLabelIcon(labelRow, info);
            witoModal.addLabelMessage(labelRow, info);
            return labelRow;
        });
        labelContainer.append(labels);
        return labelContainer;
    },

    addLabelMessage: (labelRow, labelInfo) => {
        labelRow.append(
            `<p class="wito-label-message"><strong>${labelInfo.name}. </strong>  ${labelInfo.message} </p>`
        );
    },

    addLabelIcon: (labelRow, labelInfo) => {
        labelRow.append(
            `<img class="wito-label-icon" src=${witoModal.labelToIcon(
                labelInfo.id
            )}></img>`
        );
    },

    labelToIcon: (label) => {
        let url = "";
        try {
            url = witoHTML.categoryToIcon[label];
        } catch (err) {
            url = "icons/shield-grey.svg";
            console.log("Error: ", err);
            console.warn(`No icon was found for category ${label}.`);
        }
        return chrome.extension.getURL(url);
    },

    addFooter: (modal) => {
        modal.append(
            `<footer class="wito-modal-footer"> <p class="wito-footer-text"> For more information, visit <a class="wito-footer-text" href="https://myguard.online" target="_blank"> myguard.online</a> <p/></footer>`
        );
    },
};
