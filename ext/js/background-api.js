const sendMessageToBackground = async (message) => {
    return new Promise((resolve) => {
        return chrome.runtime.sendMessage(
            null,
            message,
            null,
            function (response) {
                resolve(response);
            }
        );
    });
};

const backgroundAPI = {
    async getShorts() {
        console.log("Get short urls");
        const message = {
            operation: "getShorts",
        };

        return await sendMessageToBackground(message);
    },

    async getStoredLanguage() {
        const message = {
            operation: "getStoredValue",
            key: "translation_selected",
        };
        const response = await sendMessageToBackground(message);
        return response ? response : "sv";
    },

    async unshortenLinks(shortLinks) {
        const message = {
            operation: "expandLinks",
            shortLinks: shortLinks,
        };
        return await sendMessageToBackground(message);
    },

    async getDomainInfo(url) {
        url = url2Path(url);
        const response = await this.getLinkInfo([url]);
        return response.length > 0 ? response[0] : undefined;
    },

    async getLinkInfo(links) {
        const message = {
            operation: "getUrlInfo",
            urls: links,
        };

        return await sendMessageToBackground(message);
    },

    async getWitoLabels() {
        const message = {
            operation: "getWitoLabels",
        };
        return await sendMessageToBackground(message);
    },

    async getConfigFile() {
        const message = {
            operation: "getConfigFile",
        };
        return await sendMessageToBackground(message);
    },
};
