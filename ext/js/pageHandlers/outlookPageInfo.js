function OutlookPageInfo() {
    this.domain = "outlook.office.com";

    this.links = witoHTML.getOutlookElements();

    this.extractor = (linkElem) => [linkElem, linkElem.getAttribute("title")];
}
