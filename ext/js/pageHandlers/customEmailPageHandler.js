function CustomEmailPageHandler(pageInfo, siteInfo) {
    this.matches = (siteInfo) => {
        return siteInfo.siteDomain === pageInfo.domain;
    };

    this.getLinks = () => {
        const linkElements = pageInfo.getLinks();
        const extractor = pageInfo.extractor;
        witoHTML.setLinkCheckedAttribute(linkElements, (linkElem) => [
            linkElem,
            "",
        ]);

        const emailLinkHandler = new LinkHandlerWrapper(new EmailLinkHandler());

        for (const element of linkElements) {
            const [key, value] = extractor(element);
            if (emailLinkHandler.matches(value)) {
                emailLinkHandler.add(element, value);
            }
        }

        return emailLinkHandler.execute(siteInfo);
    };
}
