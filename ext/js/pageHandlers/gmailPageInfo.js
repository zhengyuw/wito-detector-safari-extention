function GmailPageInfo() {
    this.domain = "mail.google.com";

    this.getLinks = () => witoHTML.getGmailElements();

    this.extractor = (linkElem) => [linkElem, linkElem.getAttribute("email")];
}
