const witoAnimations = {
    colorAnimation: (element, animationSpeed, color) => {
        element.animate(
            {
                backgroundColor: color,
            },
            animationSpeed,
            "linear"
        );
    },

    setDefaultIconState: (icons) => {
        icons.forEach((element) => {
            $(element).css("opacity", "0");
        });

        $(icons[0]).css("opacity", "1");
    },

    animatePopup: (icons) => {
        let animationIndex = 0;
        const animationSpeed = 1000;
        const animateCallback = () => {
            witoAnimations.iconAnimation(
                icons[animationIndex],
                animationSpeed,
                animateCallback
            );
            animationIndex = (animationIndex + 1) % icons.length;
        };

        animateCallback();
    },

    iconAnimation: (element, animationSpeed, callback) => {
        $(element).fadeTo(animationSpeed, 1, () => {
            $(element).fadeTo(animationSpeed, 0, callback);
        });
    },
};
