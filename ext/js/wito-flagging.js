const witoFlagging = {
    flagLink: (element, linkAnchor, labelInfo) => {
        if (linkAnchor.hasClass("wito-alert-popup")) {
            return;
        }
        const flagHtml = $(`<div class="wito-alert-popup">

                            ${witoFlagging.getIcons(
                                labelInfo.map((label) => label.level)
                            )}

                </div>`);

        linkAnchor.before(flagHtml);

        const popupDimens = {
            width: $(".wito-alert-icon").width(),
            height: $(".wito-alert-icon").height(),
        };
        witoFlagging.setPopupPosition(element, flagHtml, popupDimens);
        witoFlagging.setPopupModal(
            element,
            flagHtml,
            labelInfo,
            popupDimens.width
        );
        witoFlagging.setPopupIcon(flagHtml);
    },

    setPopupPosition: (element, flagHtml, popupDimens) => {
        const positionedElement = witoFlagging.getClosestPositionedElement(
            element
        );
        $(window).resize(() => {
            flagHtml.css(
                witoFlagging.calculatePopupPosition(
                    element,
                    positionedElement,
                    popupDimens
                )
            );
        });
        const css = witoFlagging.calculatePopupPosition(
            element,
            positionedElement,
            popupDimens
        );
        flagHtml.css(css);
    },

    setPopupModal: (element, flagHtml, labelInfo, popupWidth) => {
        flagHtml.mouseenter(() => {
            manageModal.onPopupMouseEnter(
                element,
                labelInfo,
                flagHtml.offset(),
                popupWidth
            );
        });
        flagHtml.mouseleave(() => {
            manageModal.onPopupMouseLeave(element);
        });
    },

    setPopupIcon: (flagHtml) => {
        const iconElements = flagHtml.children().toArray();

        witoAnimations.setDefaultIconState(iconElements);
        witoAnimations.animatePopup(iconElements);
    },

    getIcons: (levels) => {
        const uniqueLevels = [...new Set(levels)];

        return uniqueLevels
            .map((level) => {
                const iconUrl = witoFlagging.levelToIcon(Number(level));
                return `<img class="wito-alert-icon" src="${iconUrl}"></img>`;
            })
            .join("");
    },

    popupFarFromElement: (link, nearestPositionedElement) => {
        const parentWidth = nearestPositionedElement.width();
        const linkRight = link.width() + link.position().left;
        return parentWidth - linkRight > 40;
    },

    getPopupAnchorPosition: (link) => {
        const anchorCandidates = link.children(":header");
        if (anchorCandidates.length == 0) {
            return {
                top: link.position().top,
                left: link.position().left,
                width: link.outerWidth(),
            };
        }
        let anchor = anchorCandidates.first();
        let topPadding = parseInt(anchor.css("padding-top"));
        topMargin = isNaN(topPadding) ? 0 : topPadding;
        return {
            top: anchor.position().top + topPadding,
            left: anchor.position().left,
            width: anchor.outerWidth(),
        };
    },

    calculatePopupPosition: (link, positionedElement, popupDimens) => {
        const nearestPositionedElement = link.parent().is(positionedElement)
            ? link.parent()
            : positionedElement;
        if (witoFlagging.popupFarFromElement(link, nearestPositionedElement)) {
            const anchorPosition = witoFlagging.getPopupAnchorPosition(link);
            return {
                top: anchorPosition.top + "px",
                left:
                    anchorPosition.left +
                    anchorPosition.width +
                    popupDimens.width / 2 +
                    "px",
                right: "auto",
            };
        } else {
            return {
                right: 0,
                left: "auto",
                top: link.position().top + "px",
            };
        }
    },

    getClosestPositionedElement: (link) => {
        var parent = link.parent();
        while (parent && parent.css("position") === "static") {
            if (parent.prop("nodeName") === "HTML") {
                parent = link.parent();
                break;
            }
            parent = parent.parent();
        }
        return parent;
    },

    flagSite: (labelInfos) => {
        $(".wito-alert").remove();
        const labelInfo = labelInfos[0];
        const gradient = witoFlagging.levelToGradient(
            labelInfos.map((info) => info.level)
        );
        $("body").parent().append(`<div class="wito-alert"></div>`);
        $(".wito-alert").css(gradient);
        $(".wito-alert").append(
            `<img class="wito-alert-logo-icon" src=${chrome.extension.getURL(
                "icons/myguard-white.svg"
            )}></img>`
        );
        if (labelInfos.length > 1) {
            $(".wito-alert").append(
                `<span><b>${labelInfos.map(
                    (label) => ` ${label.name}`
                )}</b> &nbsp;</span>`
            );
        } else {
            $(".wito-alert").append(
                `<span><b>${labelInfo.name}:</b> &nbsp;${labelInfo.message}</span>`
            );
        }

        $(".wito-alert").append(
            `<button class="wito-alert-close wito-button"><img class="wito-close-icon" src=${chrome.extension.getURL(
                "icons/close_white.png"
            )}></img></button>`
        );

        $(".wito-alert-close").on("click", function () {
            $(".wito-alert").remove();
        });

        $(".wito-alert").on("click", () => {
            manageBannerModal.openModal(labelInfos);
        });
    },

    levelToIcon: (level) => {
        let url = "";
        switch (level) {
            case 1:
                url = "icons/levels/shield-red.svg";
                break;
            case 2:
                url = "icons/levels/shield-black.svg";
                break;
            case 3:
                url = "icons/levels/shield-yellow.svg";
                break;
            case 4:
                url = "icons/levels/shield-blue.svg";
                break;
            case 5:
                url = "icons/levels/shield-grey.svg";
                break;
            case 6:
                url = "icons/levels/shield-green.svg";
                break;
            case 7:
                url = "icons/levels/badge-member.svg";
                break;
            default:
                url = "icons/levels/shield-grey.svg";
        }

        return chrome.extension.getURL(url);
    },

    levelToColor: (level) => {
        let color = "";
        switch (level) {
            case 1:
                //red
                color = "#e42f2f";
                break;
            case 2:
                //black
                color = "#000000";
                break;
            case 3:
                //yellow
                color = "#f4b515";
                break;
            case 4:
                //blue
                color = "#319edd";
                break;
            case 5:
                //grey
                color = "#ababab";
                break;
            case 6:
                //green
                color = "#24af18";
                break;
            case 7:
                //dark green
                color = "#006643";
                break;
            default:
                //grey
                color = "#ababab";
        }
        return color;
    },

    levelToGradient: (levels) => {
        var occurrences = levels.reduce(function (result, level) {
            result[level] = (result[level] || 0) + 1;
            return result;
        }, {});

        if (Object.keys(occurrences).length === 1) {
            return {
                background: `${witoFlagging.levelToColor(Number(levels[0]))}`,
            };
        }

        const colorRatio = Object.entries(occurrences).map(([key, value]) => {
            return [
                witoFlagging.levelToColor(Number(key)),
                (value / levels.length) * 100,
            ];
        });

        const last = colorRatio.pop();
        const gradient = {
            background: `linear-gradient(90deg, ${colorRatio.map(
                ([key, value], index) =>
                    `${key}, ${
                        index > 0 ? value + colorRatio[index - 1][1] : value
                    }%`
            )}, ${last[0]})`,
        };

        return gradient;
    },

    getLabelLink: (label) => {
        let name = label.name.charAt(0).toUpperCase() + label.name.slice(1);
        return `<a target="_blank" href="${label.url}">${name}</a>`;
    },
};
