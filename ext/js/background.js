/*!
 * B.S. Detector v0.2.7 (http://bsdetector.tech)
 * Copyright 2016 The B.S. Detector Authors (https://github.com/selfagency/bs-detector/graphs/contributors)
 * Licensed under LGPL-3.0 (https://github.com/selfagency/bs-detector/blob/master/LICENSE)
 */

if (typeof chrome !== "undefined") {
    browser = chrome;
}

const shorts = [
    "bit.do",
    "bit.ly",
    "cutt.us",
    "goo.gl",
    "ht.ly",
    "is.gd",
    "ow.ly",
    "po.st",
    "tinyurl.com",
    "tr.im",
    "trib.al",
    "u.to",
    "v.gd",
    "x.co",
    "t.co",
];

let BASE_URL = "";
let configFile;

const init = async () => {
    const file = await xhReq(browser.runtime.getURL("config/config.json"));
    configFile = JSON.parse(file);

    BASE_URL = configFile.remote.BASE_URL;
};

function xhReq(url, METHOD = "GET", data = null) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType("application/json");
        xhr.timeout = 3000;
        xhr.open(METHOD, url, true);

        xhr.onload = function () {
            if (xhr.status === 200) {
                resolve(xhr.responseText);
            } else {
                console.error("Rejecting request", url);
                reject(xhr.responseText);
            }
        };

        xhr.ontimeout = function () {
            reject({ success: false });
        };

        xhr.send(data);
    });
}

const sendWitoLabelsRequest = async () => {
    const endpoint = BASE_URL + "/categories?type=default_categories";
    const response = await xhReq(endpoint);
    return JSON.parse(response);
};

const sendUrlInfoRequest = async (urlsToFetch) => {
    const endpoint = BASE_URL + "/sites";
    const data = JSON.stringify({ urlArr: urlsToFetch });
    console.log("Sending data", data);
    const response = await xhReq(endpoint, "POST", data);
    return response;
};

const getDomainInfo = async (urls) => {
    const urlsToFetch = urls.filter((url) => !cache.contains(url2Domain(url)));
    if (urlsToFetch.length > 0) {
        const jsonResponse = JSON.parse(await sendUrlInfoRequest(urlsToFetch));

        urlsToFetch.forEach((url) => cache.insert(url2Domain(url), {}));
        jsonResponse.forEach((responseItem) =>
            cache.insert(url2Domain(responseItem.siteId), responseItem)
        );
    }

    return urls
        .map((url) => cacheTraverse.getCachedSiteInfo(cache.cacheObj, url))
        .filter((urlInfo) => urlInfo.categories);
};

async function unshortenMeAPI(urlToExpand) {
    console.log("UnshortenME API");

    var requestUrl =
        "https://unshorten.me/json/" + encodeURIComponent(urlToExpand);
    try {
        let response = JSON.parse(await xhReq(requestUrl));
        console.log("Response unshortenME", response);
        if (response.success) {
            return {
                success: true,
                requested_url: urlToExpand,
                resolved_url: url2Path(
                    decodeURIComponent(response.resolved_url)
                ),
            };
        }
    } catch (error) {
        console.log(error);
    }

    return {
        success: false,
        requested_url: "",
        resolved_url: "",
    };
}

async function expandUrlAPI(urlToExpand) {
    console.log("ExpandUrl API");
    const requestUrl =
        "http://expandurl.com/api/v1/?url=" +
        encodeURIComponent(urlToExpand) +
        "&format=json&detailed=true";
    try {
        const response = await xhReq(requestUrl);
        console.log("Response expandUrlAPI", JSON.parse(response));
        return {
            success: true,
            requested_url: urlToExpand,
            resolved_url: url2Path(
                JSON.parse(response).rel_meta_refresh[0].url
            ),
        };
    } catch (error) {
        console.log(error);
        return {
            success: false,
            requested_url: "",
            resolved_url: "",
        };
    }
}

function expandLinks(shortLinks) {
    return Promise.all(
        shortLinks.filter((link) => link).map((link) => expandLink(link))
    );
}

async function expandLink(url) {
    let response = await unshortenMeAPI(url);
    if (!response.success) {
        response = await expandUrlAPI(url);
    }

    console.log("Returning expand link", response);
    return response;
}

const getStoredValue = (key) => {
    return new Promise((resolve, reject) => {
        browser.storage.local.get([key], function (result) {
            resolve(result[key]);
        });
    });
};

const handleOnMessage = async (request) => {
    console.log(request);

    switch (request.operation) {
        case "getShorts":
            return shorts;

        case "expandLinks":
            response = await expandLinks(request.shortLinks);
            console.log("Sending response from expandlinks", response);
            return { expandedLinks: response };

        case "getUrlInfo":
            response = await getDomainInfo(removeDuplicates(request.urls));
            console.log("Sending response from url info", response);
            return response;
        case "togglePopup":
            await browser.tabs.getSelected(null, function (tabs) {
                console.log("Tabs", tabs);
                browser.tabs.sendMessage(tabs.id, request);
            });
            break;
        case "rateLabels":
            console.log("request: ", request.data);
            break;
        case "getWitoLabels":
            response = await sendWitoLabelsRequest();
            return response;
        case "storeValue":
            const setKey = request.key;
            const value = request.value;

            browser.storage.local.set({ [setKey]: value }, function () {
                console.log("Value is set to " + value);
            });

            break;

        case "getStoredValue":
            const getKey = request.key;
            return await getStoredValue(getKey);
            break;
        case "getConfigFile":
            return configFile;
    }
};

browser.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    handleOnMessage(request).then((response) => sendResponse(response));
    return true;
});

// toggle display of the warning UI when the pageAction is clicked
browser.browserAction.onClicked.addListener(function (tab) {
    "use strict";

    xhReq(browser.extension.getURL("./html/popup.html")).then((htmlFile) => {
        console.log("togglePopup");

        const response = {
            operation: "togglePopup",
            data: htmlFile,
        };

        browser.tabs.sendMessage(tab.id, response);
    });

    return true;
});

browser.storage.onChanged.addListener(function (changes, namespace) {
    console.log("storage changed");

    for (var key in changes) {
        if (key === "translation_selected") {
            browser.tabs.query(
                { active: true, currentWindow: true },
                function (tabs) {
                    browser.tabs.sendMessage(tabs[0].id, {
                        operation: "languageChange",
                    });
                }
            );
        }
    }
});

init();
