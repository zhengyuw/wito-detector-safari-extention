const cache = {
    cacheObj: {},

    hasExpired: (cachedTime, domain) => {
        const KEEP_IN_CACHE_TIME_MS = 60 * 60 * 1000;
        return Date.now() - cachedTime > KEEP_IN_CACHE_TIME_MS;
    },

    contains: (domain) => {
        return (
            domain in cache.cacheObj &&
            !cache.hasExpired(cache.cacheObj[domain].cacheDate, domain)
        );
    },

    get: (domain) => {
        return cache.cacheObj[domain];
    },

    insert: (domain, value) => {
        cache.cacheObj[domain] = value;
        cache.cacheObj[domain].cacheDate = Date.now();
    },
};
