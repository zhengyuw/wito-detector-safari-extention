function PhoneNumberHandler() {
    this.numberRegExp = new RegExp(/^tel:(.*)/);

    this.matches = (link) => {
        const numberLink = this.numberRegExp.exec(link);
        return numberLink && numberLink[1];
    };

    this.extract = (link) => {
        link = this.numberRegExp.exec(link)[1];
        return [decodeURIComponent(link).replace(/\D/g, "")];
    };

    this.linkAPI = (links) => {
        return backgroundAPI.getLinkInfo(links);
    };
}
