function FBLinkHandler() {
    this.fbRegExp = new RegExp(
        /^https?:\/\/l\.facebook\.com\/l\.php\?u=([^&]+)/
    );

    this.matches = (link) => {
        const fbLink = this.fbRegExp.exec(link);
        return fbLink && fbLink[1];
    };

    this.extract = (link) => {
        return [this.getFbLink(link)];
    };

    this.getFbLink = (link) => {
        return url2Path(decodeURIComponent(this.fbRegExp.exec(link)[1]));
    };

    this.linkAPI = (links) => {
        return backgroundAPI.getLinkInfo(links);
    };
}
