function ShortLinkHandler(shorts) {
    this.shortLinkPatterns = shorts;

    this.matches = (link) => {
        return this.shortLinkPatterns.indexOf(url2Domain(link)) > -1;
    };

    this.extract = (link) => {
        return [link];
    };

    this.linkAPI = async (shortenedLinks) => {
        const unshortenedPromises = shortenedLinks.flatMap(
            async (shortLink) => {
                const response = await backgroundAPI.unshortenLinks([
                    shortLink,
                ]);
                const links = response.expandedLinks.map(
                    (expanded) => expanded.resolved_url
                );
                return (await backgroundAPI.getLinkInfo(links)).map(
                    (linkInfo) => {
                        linkInfo.siteId = shortLink;
                        return linkInfo;
                    }
                );
            }
        );

        return Promise.all(unshortenedPromises).then((response) => {
            return response.filter((e) => e && e[0]).flatMap((e) => e[0]);
        });
    };
}
