function LinkHandlerWrapper(linkHandler) {
    this.addedElements = {};

    this.matches = (link) => {
        return linkHandler.matches(link);
    };

    this.add = (element, href) => {
        const links = linkHandler.extract(href);
        links.forEach((link) => {
            if (!(link in this.addedElements)) {
                this.addedElements[link] = [];
            }
            this.addedElements[link].push(element);
        });
    };

    this.execute = async (siteInfo) => {
        const filteredKeys = this.filterSubDomainLinks(
            siteInfo,
            Object.keys(this.addedElements)
        );
        return linkHandler.linkAPI(filteredKeys).then((labeledLinks) => {
            return labeledLinks.flatMap((linkInfo) => {
                return this.addedElements[linkInfo.siteId].map((element) => ({
                    "wito-info": linkInfo,
                    element: element,
                }));
            });
        });
    };

    this.filterSubDomainLinks = (siteInfo, links) => {
        return links.filter((link) => !isSubUrl(link, siteInfo.labeledUrl));
    };
}
