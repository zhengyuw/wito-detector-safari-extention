function EmailLinkHandler() {
    this.emailRegExp = new RegExp(/^mailto:(.*)/);

    this.matches = (link) => {
        const emailLink = this.emailRegExp.exec(link);
        return (emailLink && emailLink[1]) || link.includes("@");
    };

    this.extract = (link) => {
        if (link.includes("@")) {
            return [emailToDomain(link), link];
        } else {
            return [this.getEmailLink(link), this.emailRegExp(link)[1]];
        }
    };

    this.getEmailLink = (link) => {
        return url2Path(emailToDomain(this.emailRegExp.exec(link)[1]));
    };

    this.linkAPI = (links) => {
        return backgroundAPI.getLinkInfo(links);
    };
}
