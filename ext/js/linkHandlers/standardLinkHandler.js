function StandardLinkHandler() {
    this.matches = (link) => {
        return true;
    };
    this.extract = (link) => {
        return [url2Path(link)];
    };
    this.linkAPI = (links) => {
        return backgroundAPI.getLinkInfo(links);
    };
}
