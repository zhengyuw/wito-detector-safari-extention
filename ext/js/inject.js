const inject = {
    link: (path) => {
        $("head").append(
            $("<link>")
                .attr("rel", "stylesheet")
                .attr("type", "text/css")
                .attr("href", path)
        );
    },

    script: (path) => {
        $("head").append($("<script>").attr("src", path));
    },
};
