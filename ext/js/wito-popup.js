// The keys should match the languages in the category/level-database.
const LANGUAGE_MAP = { en: "🇬🇧 English", sv: "🇸🇪 Svenska" };

const init = async () => {
    let options = document.querySelector("#translations");
    options.addEventListener("change", onOptionChange);
    const languages = Object.keys(LANGUAGE_MAP);
    const translation = await getTranslation();
    for (let i = 0; i < languages.length; i++) {
        var opt = document.createElement("option");
        const language = languages[i];
        opt.value = language;
        opt.text = LANGUAGE_MAP[language];
        if (translation == language) {
            opt.selected = true;
        }
        options.add(opt);
    }
    addLogoImg();
};

const addLogoImg = () => {
    const logoImg = document.querySelector("#popuplogo");
    logoImg.src = chrome.extension.getURL("icons/myguard-black.svg");
};

const getTranslation = async () => {
    const message = {
        operation: "getStoredValue",
        key: "translation_selected",
    };
    const response = await sendMessageToBackground(message);
    return response ? response : "sv";
};

const onOptionChange = () => {
    let options = document.querySelector("#translations");
    console.log("option change");
    const message = {
        operation: "storeValue",
        key: "translation_selected",
        value: options.value,
    };
    sendMessageToBackground(message);
};

const sendMessageToBackground = async (message) => {
    console.log("sending message in popup");
    return new Promise((resolve) => {
        return chrome.runtime.sendMessage(
            null,
            message,
            null,
            function (response) {
                resolve(response);
            }
        );
    });
};

init();
