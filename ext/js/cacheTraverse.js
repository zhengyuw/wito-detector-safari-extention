const cacheTraverse = {
    counter: 0,

    getCachedSiteInfo: (cache, url) => {
        const siteInfo = { siteId: url };
        const [head, ...rest] = url.split("/");
        return cacheTraverse.recursive(cache[head], rest, siteInfo, [head]);
    },

    recursive: (cache, urlArray, result, labeledUrl) => {
        if (!cache) {
            return result;
        }

        const [head, ...rest] = urlArray;

        if (cache["categories"]) {
            result = {
                ...result,
                categories: cache["categories"],
                labeledUrl: labeledUrl.join("/"),
            };
        }

        if (!head || !cache.site || !cache.site[head]) {
            return result;
        } else {
            return cacheTraverse.recursive(cache.site[head], rest, result, [
                ...labeledUrl,
                head,
            ]);
        }
    },

    cacheSiteInfo: (cache, urlArray, siteInfo) => {
        if (!cache.contains(url2Domain(urlArray))) {
            cache.insert(url2Domain(urlArray), {});
        }
        return cacheTraverse.recursiveCreate(
            cache.cacheObj,
            urlArray.split("/"),
            siteInfo
        );
    },

    recursiveCreate: (cache, urlArray, siteInfo) => {
        const [head, ...rest] = urlArray;

        if (!head) {
            cacheTraverse.copyAttributes(siteInfo, cache);
            return cache;
        }

        if (!cache.site || !cache.site[head]) {
            cacheTraverse.counter += 1;

            if (!cache.site) {
                cache.site = {};
            }

            cache.site[head] = {};
        }

        return cacheTraverse.recursiveCreate(cache.site[head], rest, siteInfo);
    },

    copyAttributes: (from, to) => {
        for (var k in from) {
            to[k] = from[k];
        }
    },
};
